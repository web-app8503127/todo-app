const express=require('express');
const session=require('express-session');
const checkAuth=require('./middlewares/checkAuth');
const ejs=require('ejs');
const multer=require('multer');
const upload=multer({dest: 'uploads/'})
const fs=require('fs');
const app=express();
const port=3000;

app.set('view engine','ejs');

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
}));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static('uploads'));
app.use(express.static('public'));

app.get('/sid', (req, res) => {
	res.json({ss: req.session.id});
})

app.get("/",checkAuth,(req,res)=>{
    res.render("index.ejs",{username: req.session.username, profile: req.session.profile});
});

app.get('/signupError',(req,res)=>{
    res.sendFile(__dirname+"/public/signup/error.html");
})

app.get('/signupSuccess',(req,res)=>{
    res.sendFile(__dirname+"/public/signup/success.html");
})

app.get('/loginError',(req,res)=>{
    res.sendFile(__dirname+"/public/login/error.html");
})

app.route("/login")
.get((req,res)=>{
    if(req.session.is_logged_in)
        res.redirect('/');
    else
        res.sendFile(__dirname+"/public/login/login.html");
})
.post((req,res)=>{
    readFile(__dirname+"/public/data/user.json",(err,users)=>{
        if(users==="")
            users='[]';
        users=JSON.parse(users);
        for (let user of users) {
            if (user.email === req.body.email && user.password === req.body.password) {
                req.session.is_logged_in=true;
                req.session.username=user.fname;
                req.session.email=user.email;
                req.session.profile=user.profile; 
                res.redirect('/login');
                return;
            }
        } 
        res.redirect('/loginError');
    });
})

app.post('/signup',upload.single("profile_pic"),(req,res)=>{
    readFile(__dirname+"/public/data/user.json",(err,users)=>{
        if(users==="")
            users='[]';
        users=JSON.parse(users);
        for (let user of users) {
            if (user.email === req.body.email) {
                res.redirect('/signupError');
                return; 
            }
        }
        req.body.profile=req.file.filename;
        users.push(req.body);
        writeFile(__dirname+"/public/data/user.json",users,(err)=>{
            if(err){
                console.log("Error while writing file: ",err);
                return;
            }
            else{
                console.log("File written Successfully");
                res.redirect('/signupSuccess');
            }
        });
    });
})

app.post('/logout',(req,res)=>{
    req.session.destroy();
    res.redirect("/login");
})

app.post('/createList',checkAuth,(req,res)=>{
    if(req.body.list=="main" || req.body.list=="Main")
    {
        res.status(400).send("List cannot be created with name 'Main");
        return;
    }
    readFile(__dirname+"/public/data/list.json",(err,result)=>{
        if(result==="")
        {
            result='{}';
        }
        result=JSON.parse(result);
        if (!result[req.session.email]) {
            result[req.session.email] = [];
        }
        let data=result[req.session.email];
        for(let i in data){
            if(req.body.list==data[i]){
                console.log('Already Added!');
                res.status(400).send("List already exist");
                return;
            }
        }
        result[req.session.email].push(req.body.list);
        writeFile(__dirname+"/public/data/list.json",result,(err)=>{
            if(err){
                console.log("Error while writing file: ",err);
                return;
            }
            else{
                console.log("File written Successfully");
                res.status(200).send("List Added Successfully");
            }
        });
    });
})

app.get('/getLists',checkAuth,(req,res)=>{
    readFile(__dirname+"/public/data/list.json",(err,result)=>{
        let data=[];
        if(result==="")
        {
            result='{}';
            result=JSON.parse(result);
            data={};
        }
        else{
            result=JSON.parse(result);
            data=result[req.session.email];
        }
        res.json(data);
    });
})

app.get("/getTodo",checkAuth,(req,res)=>{
    readFile(__dirname+"/public/data/data.json",(err,result)=>{
        let data=[];
        if(result==="")
        {
            result='{}';
            result=JSON.parse(result);
            data={};
        }
        else{
            result=JSON.parse(result);
            data=result[req.session.email][req.query.value];
        }
        res.json(data);
    });
})

app.post("/savetodo",checkAuth,(req,res)=>{
    let list=req.body.list;
    delete req.body.list;
    readFile(__dirname+"/public/data/data.json",(err,result)=>{
        if(result==="")
        {
            result='{}';
        }
        let data=JSON.parse(result);
        if (!data[req.session.email]) {
            data[req.session.email] = {};
            data[req.session.email][list]=[];
        }
        else if(! data[req.session.email][list]){
            data[req.session.email][list]=[];
        }
        data[req.session.email][list].push(req.body);
        writeFile(__dirname+"/public/data/data.json",data,(err)=>{
            if(err){
                console.log("Error while writing file: ",err);
                return;
            }
            else{
                console.log("File written Successfully");
                res.end();
            }
        });
    });
});

app.put("/updateTodo",checkAuth,(req,res)=>{
    readFile(__dirname+"/public/data/data.json",(err,result)=>{
        result=JSON.parse(result);
        let data=result[req.session.email][req.body.list];
        for(let item of data){
            if (item.id == req.body.id){
                item.status=item.status=="true"?"false":"true";
                break;
            }
        } 
        result[req.session.email][req.body.list]=data;
        writeFile(__dirname+"/public/data/data.json",result,(err)=>{
            if(err){
                console.log("Error while writing file: ",err);
                return;
            }
            else{
                console.log("File written Successfully");
                res.end();
            }
        });
    });
})

app.put("/renameTodo",checkAuth,(req,res)=>{
    readFile(__dirname+"/public/data/data.json",(err,result)=>{
        result=JSON.parse(result);
        let data=result[req.session.email][req.body.list];
        for(let item of data){
            if (item.id == req.body.id){
                item.value=req.body.name;
                break;
            }
        } 
        result[req.session.email][req.body.list]=data;
        writeFile(__dirname+"/public/data/data.json",result,(err)=>{
            if(err){
                console.log("Error while writing file: ",err);
                return;
            }
            else{
                console.log("File written Successfully");
                res.end();
            }
        });
    });
})

app.delete('/deleteTodo',checkAuth,(req,res)=>{
    readFile(__dirname+"/public/data/data.json",(err,result)=>{
        result=JSON.parse(result);
        let data=result[req.session.email][req.body.list];
        for (let i = 0; i < data.length; i++) {
            if (data[i].id === req.body.id) {
                data.splice(i, 1);
                break; 
            }
        }  
        result[req.session.email][req.body.list]=data;
        writeFile(__dirname+"/public/data/data.json",result,(err)=>{
            if(err){
                console.log("Error while writing file: ",err);
                return;
            }
            else{
                console.log("File written Successfully");
                res.end();
            }
        })
    })
})

app.get('*',(req,res)=>{
    res.sendFile(__dirname+"/public/wrongRoute/index.html");
})

function readFile(filePath, callback) {
    fs.readFile(filePath, 'utf-8', function (err, result) {
        if (err) {
            console.log('Got error in reading file: ', err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
}

function writeFile(filePath, data, callback) {
    fs.writeFile(filePath, JSON.stringify(data), function (err) {
        if (err) {
            console.log('Error while writing file: ', err);
            callback(err);
        } else {
            console.log('File written successfully');
            callback(null);
        }
    });
}

app.listen(port,()=>{
    console.log(`App is running on http://localhost:${port}`)
})
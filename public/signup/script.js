const password = document.querySelector('input[type="password"]');
const eye = document.getElementById('togglePassword');
const name = document.querySelector('input[type="text"]');
const email = document.querySelector('input[type="email"]');

eye.addEventListener('click', function (e) {
    password.type=password.type=="password"?"text":"password";
    this.classList.toggle('fa-eye-slash');
});

function validateForm() {
    if(name.value=="" || email.value=="" || password.value=="")
    {
        alert("Please fill all fields");
        return false;
    }
  }
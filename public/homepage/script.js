window.onload = function() {
    loadList();
};
let leftPane=document.getElementsByClassName('left-pane')[0];
let text=document.getElementById("text");
let list=document.getElementById("list");
let logout=document.getElementById('logout');
let selectList = document.getElementById("page");
let sort = document.getElementById("order");
let increase=document.getElementById("increase");
let decrease=document.getElementById("decrease");
const searchInput = document.getElementById('search');
searchInput.addEventListener('keyup', handleSearch);
selectList.addEventListener("change", whichList);
var searchData;
pink.addEventListener('click',()=>{leftPane.style.backgroundColor=pink.value});
document.getElementById('default').addEventListener('click',()=>{leftPane.style.backgroundColor="#efefef"});
blue.addEventListener('click',()=>{leftPane.style.backgroundColor=blue.value});
increase.addEventListener('click',()=>{
    let elements = leftPane.querySelectorAll('*');
    for (let i = 0; i < elements.length; i++) {
        let computedStyle = window.getComputedStyle(elements[i]);
        let currentSize = parseFloat(computedStyle.fontSize);

        elements[i].style.fontSize = (currentSize + 1) + 'px';
    }
})
decrease.addEventListener('click',()=>{
    let elements = leftPane.querySelectorAll('*');
    for (let i = 0; i < elements.length; i++) {
        let computedStyle = window.getComputedStyle(elements[i]);
        let currentSize = parseFloat(computedStyle.fontSize);

        elements[i].style.fontSize = (currentSize - 1) + 'px';
    }
})
sort.addEventListener("change", ()=>{
    const listIndex = selectList.selectedIndex;
    const listOption = selectList.options[listIndex];
    const sortIndex = sort.selectedIndex;
    const sortOption = sort.options[sortIndex];
    let status;
    status=sortOption.value=="bydate"?false:true;
    list.innerHTML="";
    getTodo(listOption.value,status);
});

function handleSearch() {
    const searchValue = searchInput.value.trim().toLowerCase();
    const filteredData = filterData(searchValue);
    list.innerHTML="";
    if(filteredData.length)
    {
        for(let i in filteredData)
            loadContent(filteredData[i]);
    }
}

function filterData(searchValue) {
    const filteredData = [];
    
    Object.entries(searchData).forEach(([key, item]) => {
        let value=item.value.toLowerCase();
        if (key !== 'length' && value.includes(searchValue)) {
            filteredData.push(item);
        }
    });
    return filteredData;
  }

function loadList(){
    fetch("/getLists")
    .then ((response)=> response.json())
    .then ((data)=>{
        for (let i in data){
            createNewList(data[i],false);
        }
        whichList();
    })
}
function sendCreateListRequest(customValue){
    let options={
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({list: `${customValue}`})
    }
    fetch("/createList",options)
    .then((result)=>{
        return result.text();
    })
    .then((res)=>{
        if(res=="List Added Successfully")
        {
            alert("Success: "+res);
            let newOption = document.createElement("option");
            newOption.value = customValue;
            newOption.textContent = customValue;
            let lastOption = selectList.lastElementChild;
            selectList.insertBefore(newOption, lastOption);
        }
        else{
            alert("Failed: "+res);
        }
    })
    .catch((error)=>{
        console.log("Got error whil saving todo in file: ",error);
    })
}
function createNewList(customValue,status){
    if(status)
        sendCreateListRequest(customValue);
    else{
        let newOption = document.createElement("option");
        newOption.value = customValue;
        newOption.textContent = customValue;
        let lastOption = selectList.lastElementChild;
        selectList.insertBefore(newOption, lastOption);    
    }
}
function whichList(){
    const selectedIndex = selectList.selectedIndex;
    const selectedOption = selectList.options[selectedIndex];
    if (selectedOption.value === "custom") {
        let customValue = prompt("Enter custom option value:");
        if (customValue) {
            createNewList(customValue,true);
        }
        else{
            location.reload();
        }
        selectList.options[0].selected=true;
        selectedOption.value="main";
    }
    list.innerHTML="";
    getTodo(selectedOption.value);
}

logout.addEventListener('click',()=>{
    if (! confirm("Are you sure to logout?"))
        return;
    fetch('/logout',{method: "POST"})
    .then((result)=>{
        window.location.href=result.url;
    })
    .catch((err)=>{
        console.log("Error while logout: ",err)
    })
})

function getTodo(data,toSort=false){
    const params = new URLSearchParams();
    params.append('value', data);
    fetch(`/getTodo?${params}`,{
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        }
    }).then(res=>{
        return res.json();
    })
    .then((result)=>{
        if(toSort){
            const dataArray = Object.values(result);
            dataArray.sort((a, b) => a.value.localeCompare(b.value));
            result=dataArray;
        }
        searchData=result;
        for(let item of result)
            loadContent(item);
    })
    .catch((error)=>{
        console.log("Error on requesting /getTodo: ",error);
    })
}

text.onkeyup = function(evt) {

    if (evt.keyCode == 13) {
        text.value=text.value.trim();
        text.value=text.value.replace('\n','');
        if(text.value==""){
            alert("Input field is empty");
            return;
        }
        const selectedIndex = selectList.selectedIndex;
        const selectedOption = selectList.options[selectedIndex];
        const currentDate = new Date();
        const formattedDate = `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()} ${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}`;
        sendData({value: `${text.value}`, status: `false`, id: `div_ ${new Date().getTime().toString()}`, list: `${selectedOption.value}`, time: `${formattedDate}`},loadContent);
        text.value="";
    }
};

function sendData(data,callback){
    let options={
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        
        body: JSON.stringify(data)
    }
    fetch("/savetodo",options)
    .then((result)=>{
        callback(data);
    })
    .catch((error)=>{
        console.log("Got error whil saving todo in file: ",error);
    })
}

let loadContent=(item)=>{
    let div=document.createElement('div');
    div.classList.add("item");
    div.id = `${item.id}`;
    div.addEventListener('mouseenter',handleMouseEnter);
    div.addEventListener('mouseleave',handleMouseLeave);
    let title=document.createElement('span');
    let titleDiv=document.createElement('div');
    titleDiv.classList.add("titleScroll");
    title.name=`${item.id}`;
    title.innerText=`${item.value}`;
    title.style.textDecoration=`${item.status}`=="true"?"line-through":"none";
    title.style.width="2vw";
    titleDiv.appendChild(title);
    div.appendChild(titleDiv);
    
    div.innerHTML+=`<span class="tools"><time>${item.time}</time> <i class='fas fa-pen icon' name="${item.id}"></i><input type="checkbox" name="${item.id}"><i name="${item.id}">&times</i></span>`;
    const inputTag = div.querySelector(`input[type="checkbox"][name="${item.id}"]`);
    let edit=Array.from(div.getElementsByTagName("i"))[0];
    edit.addEventListener('click',editTodo);
    let cross=Array.from(div.getElementsByTagName("i"))[1];
    cross.addEventListener('click',deleteTodo);
    inputTag.checked=JSON.parse(item.status);
    inputTag.addEventListener('click', updateStatus);
    list.appendChild(div);
}

function updateStatus(){
    const selectedIndex = selectList.selectedIndex;
    const selectedOption = selectList.options[selectedIndex];
    let iid=`${this.name}`
    fetch("/updateTodo",{
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({id: `${iid}`, list: `${selectedOption.value}`})
    }).then(res=>{
        let div=document.getElementById(iid);
        const title = div.getElementsByTagName('span')[0];
        title.style.textDecoration=`${title.style.textDecoration}`=="none"?"line-through":"none";
    })   
}

function deleteTodo(){
    const selectedIndex = selectList.selectedIndex;
    const selectedOption = selectList.options[selectedIndex];
    let iid=`${this.getAttribute('name')}`;
    fetch("/deleteTodo",{
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        
        body: JSON.stringify({id: `${iid}`, list: `${selectedOption.value}`}),
    }).then(res=>{
        let div=document.getElementById(iid);
        list.removeChild(div);
    })   
}

function editTodo(){
    const selectedIndex = selectList.selectedIndex;
    const selectedOption = selectList.options[selectedIndex];
    let iid=`${this.getAttribute('name')}`;
    let newName=prompt("Enter New Name");
    if(!newName)
        return;
    
    let title=((this.parentElement).parentElement).firstElementChild;     
    fetch("/renameTodo",{
        method: "put",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({id: `${iid}`, name: `${newName}`, list: `${selectedOption.value}`}),
    }).then(res=>{
        title.innerText=`${newName}`;
    }) 
}

function handleMouseEnter(){
    this.style.transform = "scale(1.02)";
}
function handleMouseLeave(){
    this.style.transform = "scale(1)";
}